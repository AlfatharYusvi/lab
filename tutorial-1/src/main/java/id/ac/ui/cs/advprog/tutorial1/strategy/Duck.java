package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    public void setFlyBehavior(FlyBehavior fb) {
    	flyBehavior = fb;
    }
    
    public void setQuackBehavior(QuackBehavior fb) {
    	quackBehavior = fb;
    }
    
    public void swim() {
    	System.out.println("And Now I swim");
    }
    
    public abstract void display();
    
}
